# Introduction

TEAM 2 project CLASSIC SUPER COOL MEGA COOL

<!-- toc -->

Auteur : {{ book.author }}

Date de fabrication : {{ gitbook.time }} C'est maitenant fini

Point de départ : [https://gitlab.com/pages/gitbook](https://gitlab.com/pages/gitbook)

## Ebooks

* [Support en formation PDF](/gitbook-publication.pdf)
* [Support en formation EPUB](/gitbook-publication.epub)
* [Support en formation MOBI](/gitbook-publication.mobi)

![Page de garde](logo.jpg)
